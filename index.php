<!doctype html>
<html class="no-js" lang="fr">
  <head>
    <meta charset="utf-8">
    <title>1001 polices de caractère d'écriture Gratuites</title>
    <meta name="description" content="1001 polices gratuites vous offre une large sélection de polices écriture. Télécharger  des polices gratuites pour Windows et Macintosh. les polices calligraphiques ainsi que les polices de Signature sont aussi disponibles.">
    <meta name="keywords" content="polices, polices gratuites, télécharger polices gratuites, télécharger polices, 1001 polices gratuites, calligraphiques, signature">
    <meta http-equiv=Content-Type content="text/html; charset=iso-8859-1">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/favicon.ico">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- HEADER -->
    <header class="l-header">
    	<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top">
		  <div class="container-fluid">
		  <div class="row">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a id="logo" class="navbar-brand" href="/">
		      	<img src="images/logo_128.png" class="img-responsive" alt="Télécharger le top des polices de caractère gratuites">
		      </a>
		      <p class="navbar-text">1001 Free Fonts</p>

				<!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Accueil <span class="sr-only">(current)</span></a></li>
						<li><a href="#">Télécharger toutes les polices</a></li>
						<li><a href="#">1800 polices</a></li>
						<li><a href="#">Top Hebergeur web</a></li>
						<li><a href="#">Liens</a></li>
						<li><a href="#">Favoris</a></li>
					</ul>

					<form class="navbar-form" role="search">
				        <div class="form-group">
				            <div class="input-group" style="width: 200px;">
		                    	<input type="text" style="width:200px" class="form-control" placeholder="Rechercher polices..." id="searchInput">
		                   		 <span class="input-group-btn">
		                        	<button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-search"></span></button>
		                   	 	</span>
		                	</div>
				       </div>
					</form>
		    	</div><!-- /.navbar-collapse -->
		    </div><!-- navbar-header -->
		   </div> <!-- row -->
	 	 </div><!-- /.container-fluid -->
		</nav>
    </header>

    <div class="container">
    	<!-- PAGINATION HEADER -->
    	<section>
    		<div class="container t-margin l-pagination clearfix">
    			<div class="row">
				  <div class="col-lg-12">
				  	<div class="alpha pull-left">
				  		<h4>Polices gratuites organisées par ordre Alphabétique</h4>
				  	</div>
				  	<div class="btn-down">
				  		<button class="btn down btn-warning"><span class="glyphicon glyphicon-chevron-down"></span></button>
				  	</div>
				  </div><!-- /.col-lg-12 -->
				</div>
				<?php include ('includes/alphabetic.php'); ?>
			</div>
    	</section>
		
		<!-- banner and adsense ads -->
    	<section>
    		<div class="container t-margin l-ads clearfix">
    		    <div class="row">
    				<div class="col-lg-12">
    					<div class="panel panel-default">
    						<a href="http://policeecriture.com" target="_blank"><img src="images/banner1.jpg" class="img-responsive center-block" alt="1800 polices écriture"></a>
    					</div>
    					<div class="panel panel-default">
    						<?php include ('includes/adsense.php'); ?>
    					</div>
    				</div>
    			</div>
    		</div>
    	</section> 
		
		<!-- CATEGORY HEADER -->
    	<section>
    		<div class="container t-margin l-categories clearfix">
    			<?php include ('includes/categories.php'); ?>
    		</div>
    	</section>
		
		<!-- MASCOTTES ADS-->
		<section>
			<div class="container t-margin l-ads clearfix">
			    <div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<a href="http://www.mascottes.net" target="_blank"><img src="http://www.mascottes.net/wp-content/uploads/2013/11/attention-Banner.png" class="img-responsive center-block" alt="Télécharger des bandes dessinées et mascottes pour faire de la pub"></a>
						</div>
					</div>
				</div>
			</div>
		</section>

	    <!-- MAIN -->
	    <section>
	    	<div class="t-main-banner t-margin" id="main banner">
	    		<?php include ('includes/home.php'); ?>
	    	</div> <!-- end t-main -->
	    </section>
		<!-- Pagination Alphabetically-->
		<!-- <section>
			<div class="container t-margin l-pagination clearfix">
				<h4>Liste complète des polices:</h3>
				<?php include ('includes/alphabetic.php'); ?>
			</div>
		</section> -->
		
		<!-- bottom ads -->
		<section>
			<div class="container t-margin l-ads clearfix">
    		    <div class="row">
    				<div class="col-lg-12">
    					<div class="panel panel-default">
    						<?php include ('includes/adsense.php'); ?>
    					</div>
    				</div>
    			</div>
			</div>
		</section>
		<!-- 3-column-ads -->
		<section>
	    	<div class="container t-margin l-ads clearfix">
	    		<div class="row">
	    			<div class="col-lg-12">
	    				<div class="panel panel-default">
	    					<?php include ('includes/3column-ads.php'); ?>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
    	</section>
    </div><!-- end of container-->

	<!-- FOOTER -->
    <footer class="footer text-center">
		<div class="1800-policecriture message">
			<div class="message text-center">
				<label>Plus de polices?</label><a href="http://policeecriture.com" class="btn btn-primary btn-1800" target="_blank"><i class="fa fa-search-plus"></i>1800 polices</a>
			</div>
		</div>
    	<ul class="l-list-inline">
    		<li><a href="#" class="social-icon">
		    		<span class="fa-stack fa-lg">
		  				<i class="fa fa-circle fa-stack-2x"></i>
		  				<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
					</span>
				</a>
			</li>
    		<li><a href="#" class="social-icon">
	    			<span class="fa-stack fa-lg fb">
	  					<i class="fa fa-circle fa-stack-2x"></i>
	  					<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
					</span>
				</a>
    		</li>
    		<li><a href="#" class="social-icon">
		    		<span class="fa-stack fa-lg gm">
		  				<i class="fa fa-circle fa-stack-2x"></i>
		  				<i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
					</span>
				</a>
    		</li>
    	</ul>
    	<?php include ('includes/copyright.php'); ?> 
    </footer>
	<!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
        <!-- Bootstrap -->
        <script src="bower_components/bootstrap/js/affix.js"></script>
        <script src="bower_components/bootstrap/js/alert.js"></script>
        <script src="bower_components/bootstrap/js/dropdown.js"></script>
        <script src="bower_components/bootstrap/js/tooltip.js"></script>
        <script src="bower_components/bootstrap/js/modal.js"></script>
        <script src="bower_components/bootstrap/js/transition.js"></script>
        <script src="bower_components/bootstrap/js/button.js"></script>
        <script src="bower_components/bootstrap/js/popover.js"></script>
        <script src="bower_components/bootstrap/js/carousel.js"></script>
        <script src="bower_components/bootstrap/js/scrollspy.js"></script>
        <script src="bower_components/bootstrap/js/collapse.js"></script>
        <script src="bower_components/bootstrap/js/tab.js"></script>
        <!-- end Bootstrap -->
		<!-- Main JS -->
        <script src="js/main.js"></script>
</body>
</html>