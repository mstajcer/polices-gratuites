<div class="container">
	<div class="col-lg-12">
		<h1>Les polices les plus recherchées au cours des 30 derniers jours</h1>
		<!-- a yummy apology -->
		<div class="panel panel-default">
		<!--<div class="panel-heading"><a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php">Economisez temps et argent! Telecharger toutes les polices de notre site en un clic pour seulement 12 Euros - Cliquer Ici</a></div>-->
			 <div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<img src="fontdisplay/ayummyapology.gif" class="img-responsive" alt="a yummy apology">
					</div>
					
					<!-- Download buttons -->
					<div class="col-lg-3">
						<ul class="list-unstyled hidden-sm hidden-xs visible-md visible-lg">
	      					<li id="btn-download-win">                       						
	      						<a href="#" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-windows"></i>Télécharger Version Win</a>
	      					</li>
	      					<li id="btn-download-mac">
	      						<a href="#" class="btn btn-default btn-block" target="_blank"><i class="fa fa-apple"></i>Télécharger Version Mac</a>
	      					</li>
	      					<li id="btn-download-all">
	      						<a href="#" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-download"></i>Télécharger toutes les polices</a>
	      					</li>
	      					<li id="btn-overview">
	      						<a href="#" class="btn btn-info btn-block" target="_blank"><i class="fa fa-search"></i>Aperçu</a>
	      					</li>
						</ul>

						<ul class="visible-xs visible-sm">
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-windows"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-apple"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-download"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-search"></i></a></li>
				    	</ul>
					</div>
				</div>
			</div><!-- end of panel body --> 
		</div><!-- end of panel -->

		<!-- aardvark cafe -->
		<div class="panel panel-default">
		<!--<div class="panel-heading"><a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php">Economisez temps et argent! Telecharger toutes les polices de notre site en un clic pour seulement 12 Euros - Cliquer Ici</a></div>-->
			 <div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<img src="fontdisplay/aardvarkcafe.gif" class="img-responsive" alt="aardvark cafe">
					</div>
					
					<!-- Download buttons -->
					<div class="col-lg-3">
						<ul class="list-unstyled hidden-sm hidden-xs visible-md visible-lg">
	      					<li id="btn-download-win">                       						
	      						<a href="#" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-windows"></i>Télécharger Version Win</a>
	      					</li>
	      					<li id="btn-download-mac">
	      						<a href="#" class="btn btn-default btn-block" target="_blank"><i class="fa fa-apple"></i>Télécharger Version Mac</a>
	      					</li>
	      					<li id="btn-download-all">
	      						<a href="#" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-download"></i>Télécharger toutes les polices</a>
	      					</li>
	      					<li id="btn-overview">
	      						<a href="#" class="btn btn-info btn-block" target="_blank"><i class="fa fa-search"></i>Aperçu</a>
	      					</li>
						</ul>

						<ul class="visible-xs visible-sm">
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-windows"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-apple"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-download"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-search"></i></a></li>
				    	</ul>
					</div>
				</div>
			</div><!-- end of panel body --> 
		</div><!-- end of panel -->

		<!-- a charming font -->
		<div class="panel panel-default">
		<!--<div class="panel-heading"><a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php">Economisez temps et argent! Telecharger toutes les polices de notre site en un clic pour seulement 12 Euros - Cliquer Ici</a></div>-->
			 <div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<img src="fontdisplay/acharmingfont.gif" class="img-responsive" alt="a charming font">
					</div>
					
					<!-- Download buttons -->
					<div class="col-lg-3">
						<ul class="list-unstyled hidden-sm hidden-xs visible-md visible-lg">
	      					<li id="btn-download-win">                       						
	      						<a href="#" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-windows"></i>Télécharger Version Win</a>
	      					</li>
	      					<li id="btn-download-mac">
	      						<a href="#" class="btn btn-default btn-block" target="_blank"><i class="fa fa-apple"></i>Télécharger Version Mac</a>
	      					</li>
	      					<li id="btn-download-all">
	      						<a href="#" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-download"></i>Télécharger toutes les polices</a>
	      					</li>
	      					<li id="btn-overview">
	      						<a href="#" class="btn btn-info btn-block" target="_blank"><i class="fa fa-search"></i>Aperçu</a>
	      					</li>
						</ul>

						<ul class="visible-xs visible-sm">
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-windows"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-apple"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-download"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-search"></i></a></li>
				    	</ul>
					</div>
				</div>
			</div><!-- end of panel body --> 
		</div><!-- end of panel -->
		
		<!-- acme secret agent -->
		<div class="panel panel-default">
		<!--<div class="panel-heading"><a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php">Economisez temps et argent! Telecharger toutes les polices de notre site en un clic pour seulement 12 Euros - Cliquer Ici</a></div>-->
			 <div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<img src="fontdisplay/acmesecretagent.gif" class="img-responsive" alt="acme secret agent">
					</div>
					
					<!-- Download buttons -->
					<div class="col-lg-3">
						<ul class="list-unstyled hidden-sm hidden-xs visible-md visible-lg">
	      					<li id="btn-download-win">                       						
	      						<a href="#/download/?72.51.41.223/winfonts/aajaxsurrealfreak.zip" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-windows"></i>Télécharger Version Win</a>
	      					</li>
	      					<li id="btn-download-mac">
	      						<a href="#/download/?72.51.41.223/macfonts/aajaxsurrealfreak.sit" class="btn btn-default btn-block" target="_blank"><i class="fa fa-apple"></i>Télécharger Version Mac</a>
	      					</li>
	      					<li id="btn-download-all">
	      						<a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-download"></i>Télécharger toutes les polices</a>
	      					</li>
	      					<li id="btn-overview">
	      						<a href="#" class="btn btn-info btn-block" target="_blank"><i class="fa fa-search"></i>Aperçu</a>
	      					</li>
						</ul>

						<ul class="visible-xs visible-sm">
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-windows"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-apple"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-download"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-search"></i></a></li>
				    	</ul>
					</div>
				</div>
			</div><!-- end of panel body --> 
		</div><!-- end of panel -->

		<!-- action is -->
		<div class="panel panel-default">
		<!--<div class="panel-heading"><a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php">Economisez temps et argent! Telecharger toutes les polices de notre site en un clic pour seulement 12 Euros - Cliquer Ici</a></div>-->
			 <div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<img src="fontdisplay/actionis.gif" class="img-responsive" alt="action is">
					</div>
					
					<!-- Download buttons -->
					<div class="col-lg-3">
						<ul class="list-unstyled hidden-sm hidden-xs visible-md visible-lg">
	      					<li id="btn-download-win">                       						
	      						<a href="#" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-windows"></i>Télécharger Version Win</a>
	      					</li>
	      					<li id="btn-download-mac">
	      						<a href="#" class="btn btn-default btn-block" target="_blank"><i class="fa fa-apple"></i>Télécharger Version Mac</a>
	      					</li>
	      					<li id="btn-download-all">
	      						<a href="#" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-download"></i>Télécharger toutes les polices</a>
	      					</li>
	      					<li id="btn-overview">
	      						<a href="#" class="btn btn-info btn-block" target="_blank"><i class="fa fa-search"></i>Aperçu</a>
	      					</li>
						</ul>

						<ul class="visible-xs visible-sm">
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-windows"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-apple"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-download"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-search"></i></a></li>
				    	</ul>
					</div>
				</div>
			</div><!-- end of panel body --> 
		</div><!-- end of panel -->

		<!-- advert -->
		<div class="panel panel-default">
		<!--<div class="panel-heading"><a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php">Economisez temps et argent! Telecharger toutes les polices de notre site en un clic pour seulement 12 Euros - Cliquer Ici</a></div>-->
			 <div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<img src="fontdisplay/advert.gif" class="img-responsive" alt="advert">
					</div>
					
					<!-- Download buttons -->
					<div class="col-lg-3">
						<ul class="list-unstyled hidden-sm hidden-xs visible-md visible-lg">
	      					<li id="btn-download-win">                       						
	      						<a href="#" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-windows"></i>Télécharger Version Win</a>
	      					</li>
	      					<li id="btn-download-mac">
	      						<a href="#" class="btn btn-default btn-block" target="_blank"><i class="fa fa-apple"></i>Télécharger Version Mac</a>
	      					</li>
	      					<li id="btn-download-all">
	      						<a href="#" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-download"></i>Télécharger toutes les polices</a>
	      					</li>
	      					<li id="btn-overview">
	      						<a href="#" class="btn btn-info btn-block" target="_blank"><i class="fa fa-search"></i>Aperçu</a>
	      					</li>
						</ul>

						<ul class="visible-xs visible-sm">
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-windows"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-apple"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-download"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-search"></i></a></li>
				    	</ul>
					</div>
				</div>
			</div><!-- end of panel body --> 
		</div><!-- end of panel -->

		<!-- aerovias brasil -->
		<div class="panel panel-default">
		<!--<div class="panel-heading"><a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php">Economisez temps et argent! Telecharger toutes les polices de notre site en un clic pour seulement 12 Euros - Cliquer Ici</a></div>-->
			 <div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<img src="fontdisplay/aeroviasbrasil.gif" class="img-responsive" alt="aerovias brasil">
					</div>
					
					<!-- Download buttons -->
					<div class="col-lg-3">
						<ul class="list-unstyled hidden-sm hidden-xs visible-md visible-lg">
	      					<li id="btn-download-win">                       						
	      						<a href="#" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-windows"></i>Télécharger Version Win</a>
	      					</li>
	      					<li id="btn-download-mac">
	      						<a href="#" class="btn btn-default btn-block" target="_blank"><i class="fa fa-apple"></i>Télécharger Version Mac</a>
	      					</li>
	      					<li id="btn-download-all">
	      						<a href="#" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-download"></i>Télécharger toutes les polices</a>
	      					</li>
	      					<li id="btn-overview">
	      						<a href="#" class="btn btn-info btn-block" target="_blank"><i class="fa fa-search"></i>Aperçu</a>
	      					</li>
						</ul>

						<ul class="visible-xs visible-sm">
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-windows"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-apple"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-download"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-search"></i></a></li>
				    	</ul>
					</div>
				</div>
			</div><!-- end of panel body --> 
		</div><!-- end of panel -->

		<!-- agent orange -->
		<div class="panel panel-default">
		<!--<div class="panel-heading"><a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php">Economisez temps et argent! Telecharger toutes les polices de notre site en un clic pour seulement 12 Euros - Cliquer Ici</a></div>-->
			 <div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<img src="fontdisplay/agentorange.gif" class="img-responsive" alt="agent orange">
					</div>
					
					<!-- Download buttons -->
					<div class="col-lg-3">
						<ul class="list-unstyled hidden-sm hidden-xs visible-md visible-lg">
	      					<li id="btn-download-win">                       						
	      						<a href="#" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-windows"></i>Télécharger Version Win</a>
	      					</li>
	      					<li id="btn-download-mac">
	      						<a href="#" class="btn btn-default btn-block" target="_blank"><i class="fa fa-apple"></i>Télécharger Version Mac</a>
	      					</li>
	      					<li id="btn-download-all">
	      						<a href="#" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-download"></i>Télécharger toutes les polices</a>
	      					</li>
	      					<li id="btn-overview">
	      						<a href="#" class="btn btn-info btn-block" target="_blank"><i class="fa fa-search"></i>Aperçu</a>
	      					</li>
						</ul>

						<ul class="visible-xs visible-sm">
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-windows"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-apple"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-download"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-search"></i></a></li>
				    	</ul>
					</div>
				</div>
			</div><!-- end of panel body --> 
		</div><!-- end of panel -->

		<!-- airmole -->
		<div class="panel panel-default">
		<!--<div class="panel-heading"><a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php">Economisez temps et argent! Telecharger toutes les polices de notre site en un clic pour seulement 12 Euros - Cliquer Ici</a></div>-->
			 <div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<img src="fontdisplay/airmole.gif" class="img-responsive" alt="airmole">
					</div>
					
					<!-- Download buttons -->
					<div class="col-lg-3">
						<ul class="list-unstyled hidden-sm hidden-xs visible-md visible-lg">
	      					<li id="btn-download-win">                       						
	      						<a href="#" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-windows"></i>Télécharger Version Win</a>
	      					</li>
	      					<li id="btn-download-mac">
	      						<a href="#" class="btn btn-default btn-block" target="_blank"><i class="fa fa-apple"></i>Télécharger Version Mac</a>
	      					</li>
	      					<li id="btn-download-all">
	      						<a href="#" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-download"></i>Télécharger toutes les polices</a>
	      					</li>
	      					<li id="btn-overview">
	      						<a href="#" class="btn btn-info btn-block" target="_blank"><i class="fa fa-search"></i>Aperçu</a>
	      					</li>
						</ul>

						<ul class="visible-xs visible-sm">
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-windows"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-apple"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-download"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-search"></i></a></li>
				    	</ul>
					</div>
				</div>
			</div><!-- end of panel body --> 
		</div><!-- end of panel -->
		
		<!-- airstream -->
		<div class="panel panel-default">
		<!--<div class="panel-heading"><a href="http://www.policegratuit.tophebergeur.com/Toutpolices.php">Economisez temps et argent! Telecharger toutes les polices de notre site en un clic pour seulement 12 Euros - Cliquer Ici</a></div>-->
			 <div class="panel-body">
				<div class="row">
					<div class="col-lg-9">
						<img src="fontdisplay/airstream.gif" class="img-responsive" alt="airstream">
					</div>
					
					<!-- Download buttons -->
					<div class="col-lg-3">
						<ul class="list-unstyled hidden-sm hidden-xs visible-md visible-lg">
	      					<li id="btn-download-win">                       						
	      						<a href="#" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-windows"></i>Télécharger Version Win</a>
	      					</li>
	      					<li id="btn-download-mac">
	      						<a href="#" class="btn btn-default btn-block" target="_blank"><i class="fa fa-apple"></i>Télécharger Version Mac</a>
	      					</li>
	      					<li id="btn-download-all">
	      						<a href="#" class="btn btn-warning btn-block" target="_blank"><i class="fa fa-download"></i>Télécharger toutes les polices</a>
	      					</li>
	      					<li id="btn-overview">
	      						<a href="#" class="btn btn-info btn-block" target="_blank"><i class="fa fa-search"></i>Aperçu</a>
	      					</li>
						</ul>

						<ul class="visible-xs visible-sm">
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-windows"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-apple"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-download"></i></a></li>
				    		<li><a href="#" class="mobile-icon"><i class="fa fa-search"></i></a></li>
				    	</ul>
					</div>
				</div>
			</div><!-- end of panel body --> 
		</div><!-- end of panel -->
	</div>
</div>